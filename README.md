## Description


This is a sample API that contains CRUD operations for pets domain.
The API uses Nest API framework and a documentation can be found here https://docs.nestjs.com/

## Running the app

You can application locally using Docker Compose

```bash
docker-compose up
```

In case of any questions regarding the task don't hesitate to ask us.
