import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  ParseIntPipe,
} from '@nestjs/common';
import { PetsService } from './pets.service';
import { Cat } from './interfaces/cat.interface';
import { Dog } from './interfaces/dog.interface';
import { Owner } from './interfaces/owner.interface';
import { CreateCatDto } from './dto/create.cat.dto';
import { CreateDogDto } from './dto/create.dog.dto';
import { CreateOwnerDto } from './dto/create.owner.dto';

@Controller('pets')
export class PetsController {
  constructor(private readonly petsService: PetsService) {}

  @Post('/cat')
  async addCat(@Body() createCatDto: CreateCatDto) {
    await this.petsService.addCat(createCatDto);
  }

  @Post('/dog')
  async addDog(@Body() createDogDto: CreateDogDto) {
    await this.petsService.addDog(createDogDto);
  }
  @Post('/happy-owner/:id')
  async happyOwner(
    @Param('id') id: string,
    @Body() createOwnerDto: CreateOwnerDto,
  ) {
    await this.petsService.createHappyPetOwner(createOwnerDto);
  }

  @Get()
  async findAll(petType?: 'cat' | 'dog'): Promise<Array<Cat | Dog>> {
    return await this.petsService.findAll(petType);
  }

  @Get('/cats')
  async findCats(): Promise<Cat[]> {
    return await this.petsService.findAll<Cat>('cat');
  }

  @Get('/dogs')
  async findDogs(): Promise<Dog[]> {
    return await this.petsService.findAll<Dog>('dog');
  }

  @Get('/cats/:id')
  async findCat(@Param('id') id: string): Promise<Cat> {
    return await this.petsService.findCatById(id);
  }

  @Get('/dogs/:id')
  async findDog(@Param('id') id: string): Promise<Dog> {
    return await this.petsService.findDogById(id);
  }

  @Get('/cats-weight')
  async getCatsWeight(): Promise<number> {
    return await this.petsService.getCatsWeight();
  }

  @Get('/dogs-weight')
  async getDogsWeight(): Promise<number> {
    return await this.petsService.getDogsWeight();
  }

  @Get('/happy-dogs')
  async getHappyDogs(): Promise<string[]> {
    return await this.petsService.getHappyDogs();
  }

  @Get('/top-owners/:age')
  async getPetOwners(
    @Param('age', ParseIntPipe) age: number,
  ): Promise<Owner[]> {
    if (!Number.isInteger(age)) {
      return;
    }
    return await this.petsService.getTopThreePetOwnersAtAge(age);
  }

  @Get('/**/')
  getAny() {
    return 'Error retrieving recource';
  }
}
