# @main.ts{

(1) Added GlobalFilters for errors
(2) File Added --- ## error.filter.ts
(3)Line of Code --- ## app.useGlobalFilters(new ErrorFilter());

# }

# @ weight.helper.ts{

(1)added validation when weight is undefined
(2)Line of Code -----## totalWeight += weight;
(3)total weight returned should be reset for each new request

(4)Line of Code -----## totalWeight =0 (definition moved inside the function)

# }

# @pets.service.ts{

(1) @function() findAll()
(1a) line 37. changed to ----> this.DogModel.find().exec()

(2 ) @function() getCatsWeight(), getDogsWeight()
(2a) added ##NotFoundException error handling.

(3) @function() getHappyDogs
(3a)line 77, changed from ---##this.dogModel.find({}, { name: 1 }).exec(); to -----> this.dogModel.find()
(3b) added ##NotFoundException error handling.

(4) @function() createHappyPetOwner
(4a) added a new function to create happy pet owners to enable us retrieve @function() getTopThreePetOwnersAtAge

(5) @function() getTopThreePetOwnersAtAge
(5b) return the ideal petsCount;
(5c) line 143, changed petsCount:owner.\_id ------->
petsCount: owner.owners.filter(owner => owner.age == ownerAge).length,

# }

# @ pets.controller.ts{

(6) function() getPetOwners
(6a) @route '/top-owners/:age'
(6b) added @Param type (ParseIntPipe) for age
(6c) type validation for age

(6d) added new route
(7e) @Post('/top-owners')
async topOwner(@Body() createOwnerDto: CreateOwnerDto) {
await this.petsService.createHappyPetOwner(createOwnerDto);
}

# }

# @ create.owner.dto.ts {

(7)
#}

# @ create.pet.dto.ts {

(8) added @IsNotEmpty() validation
#}
