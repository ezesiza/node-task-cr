import { Dog } from './interfaces/dog.interface';
import { Cat } from './interfaces/cat.interface';

let totalWeight = 0;

export function getTotalWeight(pets: Dog[] | Cat[]): number {
  for (let { weight } of pets) {
    //check if weight exists
    if (weight === undefined) {
      weight = 0;
    }
    //reset totalWeight each time a request is sent
    if (totalWeight > 0) {
      totalWeight = 0;
    }

    totalWeight += weight;
  }
  return totalWeight;
}
