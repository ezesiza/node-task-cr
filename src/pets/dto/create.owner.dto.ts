import { IsNotEmpty, IsObject, IsInt } from 'class-validator';

export class CreateOwnerDto {
  @IsNotEmpty()
  readonly name: string;
  @IsNotEmpty()
  @IsInt()
  readonly age: number;
  cat: string;
  dog: string;
}
